//import * as PIXI from 'pixi.js' used injection in index.html from cdn
import {loadResources} from './resources'
import Game from './game'

let app = null

window.onload = () =>{
    app = new PIXI.Application ({
        height : 640,
        width : 1136,
        resolution: 1 
    })
    document.body.appendChild(app.view)
    
    loadResources (app.loader, ()=>{
        const game = new Game(app.stage)
    })
    resize() 
}
window.addEventListener('resize', resize) 

function resize(){
    if (!app){
        return
    }
    const style = app.view.style
    let scaleX, scaleY, scale, center, margin
    
    scaleX = window.innerWidth / app.view.offsetWidth
    scaleY = window.innerHeight / app.view.offsetHeight
    scale = Math.min(scaleX, scaleY)
    style.transformOrigin = '0 0'
    style.transform = `scale(${scale})`
    
    if (app.view.offsetWidth > app.view.offsetHeight) {
        center = app.view.offsetWidth * scale < window.innerWidth ? 'horizontally' : 'vertically'
    } else {
        center = app.view.offsetHeight * scale < window.innerHeight ? 'vertically' : 'horizontally'
    }

    if (center === "horizontally") {
        margin = (window.innerWidth - app.view.offsetWidth * scale) / 2
        style .marginTop = '0px'
        style .marginBottom = '0px'
        style .marginLeft = `${margin}px`
        style .marginRight = `${margin}px`
    } else {
        margin = (window.innerHeight - app.view.offsetHeight * scale) / 2
        style .marginTop = `${margin}px`
        style .marginBottom = `${margin}px`
        style .marginLeft = '0px'
        style .marginRight = '0px'
    }
    style.padding = '0'
}