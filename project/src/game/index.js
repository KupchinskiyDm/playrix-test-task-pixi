import {getResource} from '../resources'
import AnimatedObject from '../components/animatedObject'
import PlantAnimation from '../animations/plantAnimation'
import TableAnimation from '../animations/tableAnimation'
import FloatingButtonAnimation from '../animations/floatingButtonAnimation'
import BubbleButton from '../components/bubbleButton'
import BubbleAppearAnimation from '../animations/bubbleAppearAnimation'
import FadeOutAnimation from '../animations/fadeOutAnimation'
import LogoFadeInAnimation from '../animations/logoFadeInAnimation'
import OptionRadioPanel from '../components/optionsRadioPanel'
import InteriorItem from '../components/interiorItem'
import FinalFadeInAnimation from '../animations/finalFadeInAnimation'
import SlowAlphaFadeOut from '../animations/slowAlphaFadeOut'

export default class Game {
    constructor (stage) {
        this.content = new PIXI.Container()
        this.runAfterIntro = []
        // init static graphics
        this.addGraphics('background',{ x:0, y:0 }, 0)
        this.addGraphics('decor_3',{ x:161, y:203 }, 0.5)
        this.addGraphics('decor_2',{ x:904, y:67 }, 0.5)
        this.addGraphics('decor_1',{ x:314, y:479 }, 0.5)
        this.addGraphics('austin',{ x:740, y:265 }, 0.5)

        const plant_0 = new AnimatedObject({
            tag : 'decor_0',
            anchor : { x:0.5, y:0.87 },
            position : { x:1187, y:297 },
            animation : new PlantAnimation(),
            delay : 1.4
        })
        this.content.addChild(plant_0.content)
        this.runAfterIntro.push(plant_0)

        const plant_1 = new AnimatedObject({
            tag : 'decor_0',
            anchor : { x:0.5, y:0.87 },
            position : { x:517, y:140 },
            animation : new PlantAnimation(),
            delay : 1
        })
        this.content.addChild(plant_1.content)
        this.runAfterIntro.push(plant_1)

        const table = new AnimatedObject({
            tag : 'decor_4',
            anchor : { x:0.48, y:0.94 },
            position : { x:321, y:384 },
            animation : new TableAnimation(),
            delay : 0.6
        })
        this.content.addChild(table.content)
        this.runAfterIntro.push(table)

        this.stairsItem = new InteriorItem({
            defaultTag : 'stairs_default',
            optionsTags : ['stairs_0','stairs_1','stairs_2'],
            position : {x: 1160, y: 350}
        })
        this.content.addChild( this.stairsItem.content)


        this.addGraphics('decor_5',{ x:1205, y:560 }, 0.5)

        const floatingButton = new AnimatedObject({
            tag : 'continue_button',
            position : { x:695, y:560 },
            animation : new FloatingButtonAnimation()
        })
        this.content.addChild(floatingButton.content)
        floatingButton.run()

        const hammerButton = new BubbleButton({
            tag : 'bubble',
            position : { x:1139, y:322 },
            hit : {
                rect : {x : 86, y : 86},
                position : {x : -44, y : -58}
            },
            animation : new BubbleAppearAnimation(),
            disappearAnimation : new FadeOutAnimation(),
            onClick : this.onHammerButtonClick.bind(this),
            delay : 2
        })
        this.content.addChild(hammerButton.content)
        this.runAfterIntro.push(hammerButton)

        this.stairsBuildOptions = new OptionRadioPanel({
            optionsTags : ['stairs_option_0','stairs_option_1','stairs_option_2'],
            position : {x: 800, y: 250},
            onClick : this.onStairsOptionSelected.bind(this),
            onAccept : this.onstairsAccepted.bind(this)
        })
        
        this.final = new AnimatedObject({
            tag : 'final',
            position : {x : 695, y : 250},
            animation : new FinalFadeInAnimation(),
            delay : 0.2
        })
        this.content.addChild(this.final.content)

        const logo = new AnimatedObject({
            tag : 'logo',
            position : { x:290, y:75 },
            animation : new LogoFadeInAnimation()
        })
        this.content.addChild(logo.content)
        this.runAfterIntro.push(logo)
        
        const inrtoShadow = new PIXI.Graphics()
        inrtoShadow.beginFill(0x00000)
        inrtoShadow.drawRect (-5, -5, 1400, 650)
        inrtoShadow.endFill()

        this.introAnimation = new AnimatedObject({})
        this.introAnimation.graphics = inrtoShadow;
        this.introAnimation.animation = {
            animation : new SlowAlphaFadeOut(),
            onComplete : this.onIntroCompleted.bind(this)
        }
        this.content.addChild(this.introAnimation.content)
        this.introAnimation.run();  

        stage.addChild(this.content)

        this.content.position.x = -127 // to fit content according to example
    }

    onHammerButtonClick(){
        this.content.addChild(this.stairsBuildOptions.content)
        this.stairsBuildOptions.run()
    }

    onStairsOptionSelected (index){
        this.stairsItem.updateItem(index)
    }

    onstairsAccepted (index) {
        this.final.run()
    }

    addGraphics (tag, position, anchor){
        const item = this.content.addChild(new PIXI.Sprite(getResource(tag).texture))
        item.anchor.set(anchor)
        item.position = position
    }

    onIntroCompleted (){
        this.content.removeChild(this.introAnimation.content)
        this.introAnimation = null
        this.runAfterIntro.forEach(item => item.run())
    }
}