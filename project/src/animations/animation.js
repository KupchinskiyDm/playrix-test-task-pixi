export default class Animation {
    constructor () {
        this._content = null
        this._graphics = null
        this._onComplete = null
        this._timeline = new TimelineMax()
    }

    setup (content, graphics, onComplete){
        this._content = content
        this._graphics = graphics
        if (!this._graphics){
            throw new Error('Looks like graphics in not set yet to animated object. Set graphics before.')
        }
        this._onComplete = onComplete
    }

    get timeline (){
        return this._timeline
    }

    //interface
    run (){
        this._timeline.clear()
    }
}