import Animation from "./animation" 

export default class LaunchAnimation extends Animation {
        setup (content, graphics, onComplete) {
            super.setup(content, graphics, onComplete)
            this._graphics.alpha = 0
            this._graphics.position.y = -200
        }
    
        run () {
            super.run()
            this._timeline.to(this._graphics, 0.4, {ease: Power0.easeNone, pixi:{y : 0, alpha : 1}, onComplete : ()=> {
                this._onComplete && this._onComplete()
            }}) 
        }
}