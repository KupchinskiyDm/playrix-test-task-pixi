import Animation from "./animation" 

export default class SlowAlphaFadeOut extends Animation{
    setup (content, graphics, onComplete) {
        super.setup(content, graphics, onComplete)
        this._graphics.alpha = 1
    }

    run () {
        super.run()
        this._timeline.to(this._graphics, 0.55, {ease: Power0.easeNone, pixi:{alpha : 0}, onComplete : ()=> {
            this._onComplete && this._onComplete()
        }}) 
    }
}