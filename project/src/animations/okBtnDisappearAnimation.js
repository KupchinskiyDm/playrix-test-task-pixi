import Animation from "./animation" 

export default class OKBtnDisappearAnimation extends Animation {
        setup (content, graphics, onComplete) {
            super.setup(content, graphics, onComplete)
            this._graphics.alpha = 1
            this._graphics.position.y = 0
        }
    
        run () {
            super.run()
            this._timeline.to(this._graphics, 0.2, {ease: Power0.easeNone, pixi:{y : 60, alpha : 0}, onComplete : ()=> {
                this._onComplete && this._onComplete()
            }}) 
        }
}