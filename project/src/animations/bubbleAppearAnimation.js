import Animation from './animation'

export default class BubbleAppearAnimation extends Animation{
    
    setup (content, graphics, onComplete) {
        super.setup(content, graphics, onComplete)
        this._graphics.alpha = 0
        this._graphics.y = -142
    }

    run () {
        super.run()
        this._timeline.to(this._graphics, 0.25, {ease: Power0.easeNone, pixi:{y:10, alpha:1}})
            .to (this._graphics, 0.083, {ease: Power0.easeNone, pixi:{y:-10}})
            .to (this._graphics, 0.083, {ease: Power0.easeNone, pixi:{y:0}, onComplete : ()=> {
                this._onComplete && this._onComplete()
            }})
    }

}