import Animation from './animation'

export default class FloatingButtonAnimation extends Animation{
    run () {
       this._floating()
    }

    _floating (){
        super.run()
        this._timeline.to(this._graphics, 1, {ease: Power0.easeNone, pixi:{scaleX :1.08, scaleY : 1.08}})
            .to (this._graphics, 1, {ease: Power0.easeNone, pixi:{scaleX :1, scaleY : 1}, onComplete : ()=> {
                this._floating()
            }})
    }

}