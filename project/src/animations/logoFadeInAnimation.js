import Animation from './animation'

export default class LogoFadeInAnimation extends Animation{
    
    setup (content, graphics, onComplete) {
        super.setup(content, graphics, onComplete)
        this._graphics.alpha = 0
        this._graphics.scale = {x:0.01, y:0.01}
    }

    run () {
        super.run()
        TweenMax.to(this._graphics, 0.12, {ease: Power0.easeNone, pixi:{alpha : 1}})  
        this._timeline.to(this._graphics, 0.25, {ease: Power0.easeNone, pixi:{scaleX : 1.3, scaleY : 1.3}})
            .to(this._graphics, 0.15, {ease: Power0.easeNone, pixi:{scaleX : 1.02, scaleY : 1.02}})
            .to(this._graphics, 0.12, {ease: Power0.easeNone, pixi:{scaleX : 1.08, scaleY : 1.08}})
            .to(this._graphics, 0.1, {ease: Power0.easeNone, pixi:{scaleX : 1, scaleY : 1}, onComplete : ()=> {
                this._onComplete && this._onComplete()
            }}) 
    }
}