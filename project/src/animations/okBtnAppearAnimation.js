import Animation from "./animation" 

export default class OKBtnAppearAnimation extends Animation {
        setup (content, graphics, onComplete) {
            super.setup(content, graphics, onComplete)
            this._graphics.alpha = 0
            this._graphics.position.y = 60
        }
    
        run () {
            super.run()
            this._timeline.to(this._graphics, 0.2, {ease: Power0.easeNone, pixi:{y : -10, alpha : 1}}) 
                .to(this._graphics, 0.1, {ease: Power0.easeNone, pixi:{y : 8}})
                .to(this._graphics, 0.1, {ease: Power0.easeNone, pixi:{y : 0}, onComplete : ()=> {
                    this._onComplete && this._onComplete()
                }}) 
        }
}