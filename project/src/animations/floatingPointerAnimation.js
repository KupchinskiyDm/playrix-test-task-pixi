import Animation from './animation'

export default class FloatingPointerAnimation extends Animation{
    run () {
       this._floating()
    }

    _floating (){
        super.run()
        this._timeline.to(this._graphics, 1, {ease: Power0.easeNone, pixi:{x : 60, y : -10}})
            .to (this._graphics, 0.7, {ease: Power0.easeNone, pixi:{x :0, y : 0}, onComplete : ()=> {
                this._floating()
            }})
    }

}