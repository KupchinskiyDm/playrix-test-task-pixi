import Animation from './animation'

export default class FadeOutAnimation extends Animation{
    
    setup (content, graphics, onComplete) {
        super.setup(content, graphics, onComplete)
        this._graphics.alpha = 1
        this._graphics.scale = {x:1, y:1}
    }

    run () {
        super.run()
        this._timeline.to(this._graphics, 0.3, {ease: Power0.easeNone, pixi:{scaleX :0.01, scaleY : 0.01, alpha : 0}, onComplete : ()=> {
            this._onComplete && this._onComplete()
        }}) 
    }

}