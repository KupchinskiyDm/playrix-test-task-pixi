import Animation from "./animation" 

export default class SlowAlphaFadeIn extends Animation{
    setup (content, graphics, onComplete) {
        super.setup(content, graphics, onComplete)
        this._graphics.alpha = 0
    }

    run () {
        super.run()
        this._timeline.to(this._graphics, 0.55, {ease: Power0.easeNone, pixi:{alpha : 1}, onComplete : ()=> {
            this._onComplete && this._onComplete()
        }}) 
    }
}