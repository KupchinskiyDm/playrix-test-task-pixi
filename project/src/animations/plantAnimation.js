import Animation from './animation'

export default class PlantAnimation extends Animation{
    
    setup (content, graphics, onComplete) {
        super.setup(content, graphics, onComplete)
        
        this._graphics.scale = {x :0.2, y:1.3}
        this._graphics.alpha = 0
        this._graphics.y = -150
    }

    run () {
        super.run()
        this._timeline.to(this._graphics, 0.15, {ease: Power0.easeNone, pixi:{y:0, alpha:1}})
            .to (this._graphics, 0.06, {ease: Power0.easeNone, pixi:{scaleX :1.3, scaleY : 0.45}})
            .to (this._graphics, 0.19, {ease: Power0.easeNone, pixi:{scaleX :1, scaleY : 1.1}})
            .to (this._graphics, 0.07, {ease: Power0.easeNone, pixi:{scaleX :1, scaleY : 1}, onComplete : ()=> {
                this._onComplete && this._onComplete()
            }})
        
    }

}