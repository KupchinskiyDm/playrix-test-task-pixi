import Animation from './animation'

export default class TableAnimation extends Animation{
    
    setup (content, graphics, onComplete) {
        super.setup(content, graphics, onComplete)
        
        this._graphics.scale = {x :0.6, y:1.3}
        this._graphics.alpha = 0
        this._graphics.y = -40
    }

    run () {
        super.run()
        this._timeline.to(this._graphics, 0.12, {ease: Power0.easeNone, pixi:{y:0, alpha:1}})
            .to (this._graphics, 0.12, {ease: Power0.easeNone, pixi:{scaleX :1.15, scaleY : 0.8}})
            .to (this._graphics, 0.18, {ease: Power0.easeNone, pixi:{scaleX :1, scaleY : 1.1}})
            .to (this._graphics, 0.083, {ease: Power0.easeNone, pixi:{scaleX :1, scaleY : 1}, onComplete : ()=> {
                this._onComplete && this._onComplete()
            }})
        
    }

}