import manifest from '../../manifest'

let resources = null 

export const loadResources = (loader, onComplete) => {
    manifest.forEach(data => {
        loader.add(data.tag, data.source)
    })
    loader.load ((loader, res) => {
        resources = res
        onComplete && onComplete.apply()
    })
}

export const getResource = (tag) => {
    if (resources){
        if (resources.hasOwnProperty(tag)){
            return resources[tag]
        } else {
            console.error(`Cannot get [${tag}] from resources`) 
        }

    } else {
        console.error('Resources wasn`t loaded! Load resources before.') 
    }
    return null
}