/* A bit annotation for this component :  I had an idea to write adapative conponnent with dynamic amount of possible options
    with smart paddings of elements around the circle.
    However its too much complicated for test task and takes a big affrot.

    There is not best solution for this like component below:
*/

import OptionButton from "../optionButton" 
import FadeOutAnimation from "../../animations/fadeOutAnimation" 
import GameObject from "../gameObject"
import FinalFadeInAnimation from "../../animations/finalFadeInAnimation"

export default class OptionRadioPanel extends GameObject{
    constructor (params){
        super(params)
        if (!params.optionsTags || (params.optionsTags.length < 1 || params.optionsTags.length > 3)){
            throw new Error('Looks like OptionRadioPanel takes invalid amount of options')
        }
        
        const positions = [{x:0, y:0}, {x:135, y:-100}, {x:300, y:-90}]
        const delays = [0, 0.2, 0.4]
        
        this._onClick = params.onClick
        this._onAccept = params.onAccept

        this._options = params.optionsTags.map((element, index) => {
            const option = new OptionButton({
                tag : 'option_frame',
                position : positions[index],
                spriteTag : element,
                index : index,
                hit : {
                    rect : {x : 100, y : 100},
                    position : {x : -50, y : -50}
                },
                animation : new FinalFadeInAnimation(),
                disappearAnimation : new FadeOutAnimation(),
                delay : delays[index],
                onClick : this._onOptionSelected.bind(this),
                onAccept : this._onOptionAccepted.bind(this)
            })
            this._content.addChild(option.content)
            return option
        })
    }   

    _onOptionSelected (index){
        this._options.filter(item => {
            return item.index !== index && item.selected
        }).forEach(item => {
            item.selected = false
        })
        this._onClick && this._onClick(index)
    }

    _onOptionAccepted(index){
        this._options.forEach(element => {
            element.disappear()
        })
        this._onAccept && this._onAccept(index)
    }

    run () {
        this._options.forEach(element => {
            element.run()
        })
    }
}