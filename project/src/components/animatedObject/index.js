import GameObject from '../gameObject'

export default class AnimatedObject extends GameObject {
    constructor (params) {
        super(params)
        this._timeout = null
        this.animation = params
    }

    run () {
        typeof this._timeout === 'number' && clearTimeout(this._timeout)
        if (this._animation){
            if (this._delay > 0){
                this._timeout = setTimeout(this._playAnimation.bind(this), this._delay * 1000) 
            } else{
                this._playAnimation() 
            }
        }
    }

    /**
     * @param {{ animation: any;  onStarted : Function;  onComplete: Function;  delay : Number} params
     */
    set animation (params){
        this._onComplete = params.onComplete
        this._onStarted = params.onStarted
        this._animation = params.animation
        this._animation && 
            this._animation.setup (this._content, this._graphics, this._onAnimationComplete.bind(this))
        this._delay = params.delay ? params.delay  : 0
    }

    _playAnimation () {
        this._onStarted && this._onStarted()
        this._animation.run()
    }

    _onAnimationComplete () {
        this._onComplete && this._onComplete()
    }
}