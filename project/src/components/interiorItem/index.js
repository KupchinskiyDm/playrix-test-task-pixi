import GameObject from "../gameObject";
import AnimatedObject from "../animatedObject";
import LaunchAnimation from "../../animations/launchAnimation";
import { getResource } from "../../resources";

export default class InteriorItem extends GameObject{
    constructor(params){
        super(params)
        this._possibleOptions = params.optionsTags
        this._item = new AnimatedObject({
            tag : params.defaultTag
        })
        this._content.addChild(this._item.content)
        this._animation = new LaunchAnimation()
        this._textures = params.optionsTags
    }

    updateItem (index){
        this._item.animation = {
            animation : this._animation
        }
        this._item.graphics.texture = getResource(this._textures[index]).texture
        this._item.run()
    }
}