import { getResource } from "../../resources"

export default class GameObject {
    constructor(params){
        const anchor = params.anchor ? params.anchor : { x:0.5, y:0.5}

        this._content = new PIXI.Container()
        this._graphics = null

        if (params.tag) {
            const texture = getResource(params.tag).texture
            if (texture){
                this._graphics = this._content.addChild(new PIXI.Sprite(texture))
                this._graphics.anchor.set(anchor.x, anchor.y) 
            } else {
                throw new Error(`[${params.tag}] is not texture. Can not set graphics.`)
            }
        }

        this._content.position = params.position ? params.position :  { x:0, y:0}
    }

    get content (){
        return this._content 
    }

    get graphics (){
        return this._graphics
    }

    set graphics (value){
        this._graphics && this._content.removeChild(this._graphics)
        value && this._content.addChild(value)
        this._graphics = value
    }
}