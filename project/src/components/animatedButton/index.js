import AnimatedObject from "../animatedObject" 

export default class AnimatedButton extends AnimatedObject{
    constructor (params){
        super(params)
        this._onStarted = this._onAppearAnimationStart
        this._onComplete = this._onAppearAnimationEnd

        this._appearAnimation = this._animation // set default init animation as appear animation
        this._disappearAnimation = params.disappearAnimation

        this._hit = new PIXI.Graphics()
        this._hit.beginFill(0x000000)
        this._hit.drawRect (params.hit.position.x, params.hit.position.y, params.hit.rect.x, params.hit.rect.y)
        this._hit.endFill()
        this._hit.alpha = 0
        this._hit.buttonMode = true
        this._hit.interactive = false
        this._hit.cursor = 'unset' 

        this._hit.on('pointerdown', this._onPointerDown.bind(this))
        this._hit.on('pointerup', this._onPointerUp.bind(this))

        this._content.addChild(this._hit)

        this._onClick = params.onClick
    }

    _onAppearAnimationStart (){}

    _onAppearAnimationEnd (){
        this._hit.interactive = true
    }

    appear () {
        this.animation = {
            animation : this._appearAnimation, 
            onComplete : this._onAppearAnimationEnd,
        }
        this.run()
    }

    disappear () {
        this._hit.interactive = false
        this.animation = {
            animation : this._disappearAnimation, 
            onComplete : this._onDisappearAnimComplete,
        }
        this.run()
    }

    _onDisappearAnimComplete (){}

    _onPointerDown(){}

    _onPointerUp(){
        this.disappear()
        this._onClick && this._onClick.apply()
    }


}