import AnimatedButton from "../animatedButton" 
import AnimatedObject from "../animatedObject" 
import LogoFadeInAnimation from "../../animations/logoFadeInAnimation" 
import SlowAlphaFadeIn from "../../animations/slowAlphaFadeIn"
import FloatingPointerAnimation from "../../animations/floatingPointerAnimation"

export default class BubbleButton extends AnimatedButton {
    constructor (params) {
        super(params)
        this._instrument = new AnimatedObject({
            tag : params.instrTag ? params.instrTag : 'hammer_instr',
            position : {x: 5, y: -10},
            anchor : { x:0.5, y:0.5 },
            animation : new LogoFadeInAnimation(),
            delay : 0.2
        })
        this._graphics.addChild(this._instrument.content)

        this._pointer = new AnimatedObject({
            position : {x: -130, y: 30}
        })
        const pointerContent = new AnimatedObject({
            tag : 'pointer',
            animation : new FloatingPointerAnimation()
        }) 
        this._pointer.graphics = pointerContent.content;
        this._pointer.content.scale = {x : 0.5, y : 0.5}
        pointerContent.run();  
    }

    disappear () {
        super.disappear()
        this._content.removeChild(this._pointer.content)
    }

    _onAppearAnimationStart (){
        super._onAppearAnimationStart()
        this._instrument.run()
    }

    _onAppearAnimationEnd (){
        super._onAppearAnimationEnd()
        this._pointer.animation = {
            animation : new SlowAlphaFadeIn()
        }
        this._content.addChild(this._pointer.content)
        this._pointer.run();
    }
}