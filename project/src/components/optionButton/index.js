import AnimatedButton from "../animatedButton" 
import AnimatedObject from "../animatedObject" 
import AlphaFadeIn from "../../animations/alphaFadeIn" 
import AlphaFadeOut from "../../animations/alphaFadeOut" 
import OKBtnAppearAnimation from "../../animations/okBtnAppearAnimation" 
import OKBtnDisappearAnimation from "../../animations/okBtnDisappearAnimation" 
import { getResource } from "../../resources" 

export default class OptionButton extends AnimatedButton {
    constructor (params){
        super(params)
        this._index = params.index
        this._selected = false

        this._selectedFrame = new AnimatedObject({
            tag : 'selected_frame',
            anchor : { x:0.5, y:0.5 },
            position : { x:0, y:0 },
        })
        this._selectedFrame.graphics.alpha = 0 
        this._selectedFrame.content.scale = {x :0.96, y :0.96}
        this._graphics.addChild(this._selectedFrame.content)

        const sprite = this._graphics.addChild(new PIXI.Sprite(getResource(params.spriteTag).texture))
        sprite.anchor.set(0.5)
        sprite.position = {x:0, y:0}

        this._acceptButton = new AnimatedButton ({
            tag : 'ok_button',
            position : {x:0, y:90},
            hit : {
                rect : {x : 120, y : 60},
                position : {x : -60, y : -25}
            },
            animation : new OKBtnAppearAnimation(),
            disappearAnimation : new OKBtnDisappearAnimation(),
            onClick : this._onOptionAccept.bind(this)
        })
        this._graphics.addChild(this._acceptButton.content)

        this._onAccept = params.onAccept
    }

    get selected (){
        return this._selected
    }

    get index () {
        return this._index
    }

    set selected (value) {
        if (this.selected && !value){
            this._selectedFrame.animation = {
                animation : new AlphaFadeOut()
            }
            this._acceptButton.disappear()
            this._selectedFrame.run()
        }  
        
        if (!this.selected && value){
            this._selectedFrame.animation = {
                animation : new AlphaFadeIn()
            }
            this._acceptButton.appear()
            this._selectedFrame.run()
        } 
        this._selected = value
    }

    _onOptionAccept () {
        this._onAccept && this._onAccept(this._index)
    }

    _onPointerUp(){
        if (!this.selected){
            this.selected = true
            this._onClick && this._onClick(this._index)
        }
    }

}