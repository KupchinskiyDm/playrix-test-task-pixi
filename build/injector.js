// however it can be the gulp task 

const fs = require('fs')
const parser = require('node-html-parser')

const index = './dist/index.html'
const tohead = [ // add links to inject code as text to index
    {tag : 'script', path : './dist/app.bundle.js'},
    {tag : 'style', path : './dist/styles.css'}
]

let output = null
let injected = 0

if (tohead.length) {
    fs.readFile(index, function (err, html) {
        if (err) throw err
        output = parser.parse(html)
        injectNext(onComplete)
    })
}

const injectNext = (onComplete) => {
    const item = tohead[injected];
    fs.readFile(item.path, function (err, data) {
        if (err) throw err
        output.childNodes[1].childNodes[1].appendChild(`<${item.tag}>${data.toString()}</${item.tag}>`)
        injected ++
        injected < tohead.length ? injectNext(onComplete) : onComplete.apply()
    })
}

const onComplete = ()=> {
    fs.writeFile(index, output.toString(),()=>{
        tohead.forEach(item => {
            fs.unlink( item.path, (err) => {
                if (err) throw err
            })
        })
    })
}
