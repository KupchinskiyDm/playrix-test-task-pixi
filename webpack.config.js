const CopyWebpackPlugin = require('copy-webpack-plugin')
const path = require('path');

module.exports = {
    mode : 'production',
    entry: {
        app : path.resolve(__dirname, 'project/src/app.js')
    }, 
    output: {
      filename: '[name].bundle.js',
      path: path.resolve(__dirname, 'dist'),
    },
    plugins: [
        new CopyWebpackPlugin([
            {from: __dirname + '/project/index.html',to: __dirname + '/dist/index.html'},
            {from: __dirname + '/project/styles.css',to: __dirname + '/dist/styles.css'},
            {from: __dirname + '/project/res',to: __dirname + '/dist/res'} 
        ])
    ],
    module: { // for support old browser
        rules: [
            {
                test: /\.js$/,
                exclude: ["/node_modules/"],
                use: [
                    {
                        loader: "babel-loader",
                        options: {
                            presets: ["@babel/env"],
                        },
                    }
                ]
            }
        ],
    },
    performance: {
        hints: false
    },
    optimization: {
		minimize: true
	}
}