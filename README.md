# Environment

I used **Webpack** and **Babel-loader** to build sources bulnde.
Also there are a few additional build scripts in *[build]* folder. 
Generator for manifest  and Injector (for injecting bundle code into index.html) 

Just run :
        
        $ npm install

# Build dist

        $ npm run build

# For VSCode 

I prefer to use VSCode as development environment.
Launch tasks configured for **Live Server** ext. and **Debugger for Chrome** ext.

### Setup
   Run commands in terminal to install extensions (reload IDE needed) :

        $ code --install-extension msjsdiag.debugger-for-chrome
        $ code --install-extension ritwickdey.liveserver

   Press **Go Live** on bottom panel to start **Live Server**        
        
### VSCode tasks for quick launch by F5 : 

#### Gen manifest
Generate manifest.js from *[project/res]* folder to *[project]* folder
#### Build dist
Task runs command *$ npm run build* that include manifest generation, code injecting and building distribution bundle

Chrome opens automatically as new window with link http://localhost:5500 *(Live server must be active)*
